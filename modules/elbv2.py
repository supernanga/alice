#!/usr/bin/env python
"""
AWS ELBv2 module.
"""
from random import choice
import json
import boto3
from modules import logger


"""
The code below is a real-world implementation, but it isn't used by this project.
It was left here just to show how an implementation for ELBv2 / target group
health is done. This decision was made to save time, since mocking ELBv2 with
moto takes a considerable amount of time.

A hard-coded implementation was also done and its currently used by this project
to mock a random response from a target group with one or two instances. Their
# statuses can switch between "healthy" and "unhealthy".
"""


# Logging object.
log = logger.setup_logger("elbv2")


def create_session():
    """
    Creates a boto3 session.
    """
    session = boto3.session.Session()
    return session


def check_health(target_group_arn, mocked=False):
    """
    Check the health from the targets of a target group ARN,
    returning True if all targets are healthy and False if
    at least one registered target is unhealthy.
    """
    if not mocked:
        elbv2_client = create_session().client("elbv2")
        unhealthy_count = 0
        i = 0
        print("Checking target group health for '%s'..." % (target_group_arn))
        response = elbv2_client.describe_target_health(TargetGroupArn=target_group_arn)
        number_of_targets = len(response['TargetHealthDescriptions'])
        log.info("ELBv2 target group health status check", extra={
            "additional_data": {
                "target_group_arn": target_group_arn,
                "number_of_targets": number_of_targets
            }
        })
        while i < number_of_targets:
            target_status = response['TargetHealthDescriptions'][i]['TargetHealth']['State']
            if 'healthy' not in target_status:
                unhealthy_count += 1
            i += 1
        if unhealthy_count <= 0:
            log.info("ELBv2 target group health status check completed with success", extra={
                "additional_data": {
                    "target_group_arn": target_group_arn,
                    "number_of_targets": number_of_targets,
                    "unhealthy_count": unhealthy_count
                }
            })
            return True
        else:
            log.error("ELBv2 target group health status check found unhealthy target(s)", extra={
                "additional_data": {
                    "target_group_arn": target_group_arn,
                    "number_of_targets": number_of_targets,
                    "unhealthy_count": unhealthy_count
                }
            })
            return False
    else:
        return mock_check_health(target_group_arn)


def mock_check_health(target_group_arn):
    unhealthy_count = 0
    i = 0
    mocked_target_health_response = ("unhealthy", "healthy")
    mocked_response_choice = ("one", "two")
    mocked_response_one = {
        "TargetHealthDescriptions": [
            {
                "Target": {
                    "Id": "10.30.1.200",
                    "Port": 8080,
                    "AvailabilityZone": "sa-east-1a"
                },
                "HealthCheckPort": "8080",
                "TargetHealth": {
                    "State": choice(mocked_target_health_response)
                }
            }
        ],
        "ResponseMetadata": {
            "RequestId": "00438477-d6ae-4e58-85f9-8a4a62dc0639",
            "HTTPStatusCode": 200,
            "HTTPHeaders": {
                "x-amzn-requestid": "00438477-d6ae-4e58-85f9-8a4a62dc0639",
                "content-type": "text/xml",
                "content-length": "988",
                "date": "Thu, 29 Nov 2020 16:49:27 GMT"
            },
            "RetryAttempts": 0
        }
    }
    mocked_response_two = {
        "TargetHealthDescriptions": [
            {
                "Target": {
                    "Id": "10.30.1.200",
                    "Port": 8080,
                    "AvailabilityZone": "sa-east-1a"
                },
                "HealthCheckPort": "8080",
                "TargetHealth": {
                    "State": choice(mocked_target_health_response)
                }
            },
            {
                "Target": {
                    "Id": "10.30.1.227",
                    "Port": 8080,
                    "AvailabilityZone": "sa-east-1a"
                },
                "HealthCheckPort": "8080",
                "TargetHealth": {
                    "State": choice(mocked_target_health_response)
                }
            }
        ],
        "ResponseMetadata": {
            "RequestId": "00438477-d6ae-4e58-85f9-8a4a62dc0639",
            "HTTPStatusCode": 200,
            "HTTPHeaders": {
                "x-amzn-requestid": "00438477-d6ae-4e58-85f9-8a4a62dc0639",
                "content-type": "text/xml",
                "content-length": "988",
                "date": "Thu, 29 Nov 2020 16:49:27 GMT"
            },
            "RetryAttempts": 0
        }
    }
    selected_response = choice(mocked_response_choice)
    if "one" in selected_response:
        response = mocked_response_one
    elif "two" in selected_response:
        response = mocked_response_two
    number_of_targets = len(response['TargetHealthDescriptions'])
    log.info("ELBv2 target group health status check", extra={
        "additional_data": {
            "target_group_arn": target_group_arn,
            "number_of_targets": number_of_targets
        }
    })
    while i < number_of_targets:
        target_status = response['TargetHealthDescriptions'][i]['TargetHealth']['State']
        if target_status.startswith("unhealthy"):
            unhealthy_count += 1
        i += 1
    if unhealthy_count <= 0:
        log.info("ELBv2 target group health status check completed with success", extra={
            "additional_data": {
                "target_group_arn": target_group_arn,
                "number_of_targets": number_of_targets,
                "unhealthy_count": unhealthy_count
            }
        })
        return True
    else:
        log.error("ELBv2 target group health status check found unhealthy target(s)", extra={
            "additional_data": {
                "target_group_arn": target_group_arn,
                "number_of_targets": number_of_targets,
                "unhealthy_count": unhealthy_count
            }
        })
        return False
