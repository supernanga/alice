#!/usr/bin/env python
"""
Datetime utils module.
"""
import pandas as pd
from datetime import datetime
from dateutil import parser


def datetime_to_utc(datetime_string):
    """
    Converts a given datetime string (with timezone) to UTC.
    """
    # Pandas helps by converting the string out of the box.
    datetime_utc = pd.Timestamp(datetime_string).tz_convert("UTC")
    return datetime_utc


def datetime_to_epoch(datetime_string):
    """
    Converts a datetine to UNIX timestamp.
    """
    timestamp = parser.parse(datetime_string).timestamp()
    # This will return an epoch timestamp without the fraction part.
    return int(timestamp)
