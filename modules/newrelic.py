#!/usr/bin/env python
"""
New Relic module.

Metrics API:
    * https://docs.newrelic.com/docs/telemetry-data-platform/ingest-manage-data/ingest-apis/report-metrics-metric-api
    * https://docs.newrelic.com/docs/telemetry-data-platform/ingest-manage-data/understand-data/metric-data-type

Insights API:
    * https://docs.newrelic.com/docs/telemetry-data-platform/ingest-manage-data/ingest-apis/use-event-api-report-custom-events
"""
import json
from modules import logger


# Logging object.
log = logger.setup_logger("new_relic")


def format_metric(metric_name, metric_type, metric_value, metric_timestamp, attr):
    """
    Formats a metric record to JSON.
    """
    metric_log_structure = [
        {
            "metrics": [
                {
                    "name": "<PLACEHOLDER_REPLACE_ME>",
                    "type": "<PLACEHOLDER_REPLACE_ME>",
                    "value": "<PLACEHOLDER_REPLACE_ME>",
                    "timestamp": "<PLACEHOLDER_REPLACE_ME>",
                    "attributes": {}
                }
            ]
        }
    ]
    metric_log_structure[0]["metrics"][0]["name"] = metric_name
    metric_log_structure[0]["metrics"][0]["type"] = metric_type
    metric_log_structure[0]["metrics"][0]["value"] = metric_value
    metric_log_structure[0]["metrics"][0]["timestamp"] = metric_timestamp
    if "attributes" in attr:
        for key, value in attr["attributes"].items():
            metric_log_structure[0]["metrics"][0]["attributes"][key] = value
    return metric_log_structure


def push_metric(api_url, api_key, payload):
    """
    Pushes a metric to New Relic. Not implemented for the real world,
    since I don't have a New Relic account to test.

    In a real world implementation requests library will be used
    before the "log.info" line to push the metric on New Relic Metric
    API. Based in the remote API response, the log message could be
    either an informational message or an error message.
    """
    log.info("Pushing metric to New Relic", extra={
        "additional_data": {
            "status": "Not implemented, no New Relic account",
            "new_relic_api_url": api_url,
            "new_relic_api_key": api_key[:5] + "***",
            "new_relic_api_payload": payload
        }
    })


def push_custom_event(api_url, api_key, payload):
    """
    Pushes a metric to New Relic. Not implemented for the real world,
    since I don't have a New Relic account to test.

    In a real world implementation requests library will be used
    before the "log.info" line to push the metric on New Relic Metric
    API. Based in the remote API response, the log message could be
    either an informational message or an error message.
    """
    log.info("Pushing custom event to New Relic", extra={
        "additional_data": {
            "status": "Not implemented, no New Relic account",
            "new_relic_api_url": api_url,
            "new_relic_api_key": api_key[:5] + "***",
            "new_relic_api_payload": payload
        }
    })
