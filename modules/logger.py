#!/usr/bin/env python
"""
Logging module.
"""
import os
import logging
import json
import time


class JSONFormatter(logging.Formatter):
    """
    JSON formatter class.
    """
    def format(self, record):
        """
        Formats the record to JSON.
        """
        formatted_date_time = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime(record.created))
        standard_log_structure = {}
        standard_log_structure["level"] = record.levelname
        standard_log_structure["log_facility_name"] = record.name
        standard_log_structure["message"] = record.msg
        standard_log_structure["time"] = f"{formatted_date_time}.{record.msecs:3.0f}Z"
        standard_log_structure["epoch_time"] = record.created
        if hasattr(record, "additional_data"):
            for key, value in record.additional_data.items():
                standard_log_structure[key] = value
        return json.dumps(standard_log_structure)


def setup_logger(log_facility_name):
    """
    Creates logging object.
    """
    logger = logging.getLogger(log_facility_name)
    handler = logging.StreamHandler()
    formatter = JSONFormatter()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger
