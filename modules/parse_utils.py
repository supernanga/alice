#!/usr/bin/env python
"""
Simple GC log parsing module.
"""
import re
from modules import logger


# Logging object.
log = logger.setup_logger("parse_utils")


def regex_num_of_groups(regex_object):
    return re.compile(regex_object).groups


def check_valid_log(log_entry):
    """
    Checks if the log entry starts with a date.
    """
    regex = r"(\d+-\d+-\d+)"
    results = re.findall(regex, log_entry)
    if len(results) > 0:
        return True
    else:
        return False


def parse_timestamp(log_entry):
    """
    Parses the timestamp from a log entry.
    """
    regex = r"(\d+\-\d+\-\d+)T(\d+\:\d+\:\d+\.\d+\-\d+):"
    results = re.match(regex, log_entry)
    date = results.group(1)
    time = results.group(2)
    return str(date), str(time)


def parse_duration(log_entry, has_times=False):
    """
    Parses the duration of operation(s) inside a log entry.
    """
    # Log entry has "Times: user=X.XX sys=X.XX, real=X.XX secs".
    if has_times:
        regex = r"(, )(\d+.\d*)"
        results = re.findall(regex, log_entry)
        total_duration = results[0][1]
        return total_duration
    # Log entry has one or more "X.XX secs" entry(ies).
    else:
        regex = r"(\d+.\d*)(\s+secs)"
        results = re.findall(regex, log_entry)
        # Got more than one duration.
        if len(results) > 1:
            i = 0
            total_duration = 0
            while i < len(results):
                for duration in results:
                    total_duration = round(
                        total_duration + float(results[i][0]), 7)
                    i += 1
            return total_duration
        # Got just one duration.
        else:
            duration = results[0][0]
            return duration


def parse_full_gc(log_entry):
    """
    Parses a full GC log entry.
    """
    # Memory allocation details:
    # 801376K->711236K(932096K)
    #  |          |       |_> Total allocated memory space
    #  |          |_> Memory allocation after GC ran
    #  |_> Memory allocation before GC
    regex = r"\[Full GC \((.*)\) (\d+)(\w)->(\d+)(\w)\((\d+)(\w)\)"
    results = re.findall(regex, log_entry)
    full_gc_cause = results[0][0]
    memory_unit = results[0][2]
    memory_before = int(results[0][1])
    memory_after = int(results[0][3])
    total_allocated_memory = int(results[0][5])
    freed_memory = memory_before - int(memory_after)
    return full_gc_cause, freed_memory, total_allocated_memory, memory_unit


def parse_gc_pauses(log_entry):
    """
    Parses GC pauses log entries.
    """
    # Default regex seeks for three entries.
    regex = r".*?\((.*?)\).*?\((.*?)\) \((.*?)\)"
    results = re.match(regex, log_entry)
    # Got three results.
    if results is not None:
        gc_pause_action = results.group(1)
        gc_pause_collector = results.group(2)
        gc_pause_cause = results.group(3)
        return gc_pause_action, gc_pause_collector, gc_pause_cause
    # Got two results.
    else:
        regex = r".*?\((.*?)\).*?\((.*?)\)"
        results = re.match(regex, log_entry)
        gc_pause_action = results.group(1)
        gc_pause_collector = results.group(2)
        gc_pause_cause = "null"
        return gc_pause_action, gc_pause_collector, gc_pause_cause


def parse_gc_others(log_entry, remark=False):
    """
    Parses other GC entries for the following events:
        concurrent-root-region-scan-end
        concurrent-mark-end
        concurrent-cleanup-end
        Cleanup
        remark
    """
    if remark:
        pass
    else:
        regex = r"GC (\S+[a-z])"
        results = re.findall(regex, log_entry)[0]
        return results
