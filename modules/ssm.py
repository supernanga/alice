#!/usr/bin/env python
"""
SSM module (mocked).
"""
from moto import mock_ssm
from modules import logger
import boto3


# Logging object.
log = logger.setup_logger("ssm_mock")


@mock_ssm
def mock_ssm_param(ssm_parameter_name, value, secure=False):
    try:
        ssm_client = boto3.client(
            "ssm",
            aws_access_key_id="testing",
            aws_secret_access_key="testing")
        response = ssm_client.put_parameter(
            Name=ssm_parameter_name,
            Value=value,
            Type="String" if not secure else "SecureString",
            Overwrite=True
        )
        log.info("Putting SSM parameter", extra={
            "additional_data": {
                "ssm_parameter_name": ssm_parameter_name,
                "ssm_parameter_type": "String" if not secure else "SecureString"
            }
        })
        response = ssm_client.get_parameter(Name=ssm_parameter_name, WithDecryption=True)
        log.info("Getting SSM parameter", extra={
            "additional_data": {
                "ssm_parameter_name": ssm_parameter_name,
                "ssm_parameter_type": "String" if not secure else "SecureString"
            }
        })
        return response["Parameter"]["Value"]
    except Exception as error_message:
        log.error("Failure while putting SSM parameter", extra={
            "additional_data": {
                "ssm_parameter_name": ssm_parameter_name,
                "error_mesage": str(error_message)
            }
        })
        raise
