#!/usr/bin/env python
"""
JVM garbage collector log parser script.
"""
import hashlib
import os
import time
import datetime
from modules import parse_utils, dateutils, logger, ssm, elbv2, newrelic


# Variables used by the gc_log_parser script.
seek_end = True
now = datetime.datetime.now()
instance_id = "i-%s" % (hashlib.md5(str(now).encode()).hexdigest()[:17])  # Generates a fake EC2 instance ID.
#   Values from SSM Parameter Store are mocked. In a real world solution it would be
#   great to take leverage from the SSM Parameter Store.
application_name = ssm.mock_ssm_param(
    "/alice/apps/app1/name",
    "Astronotus ocellatus")  # Also known as Oscar fish ;)
environment = ssm.mock_ssm_param(
    "/alice/apps/app1/env",
    "sandbox")
log_file_name = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/filename",
    "gc.log")
mark_period = int(
    ssm.mock_ssm_param(
        "/alice/monitoring/gc-log/mark-period",
        "5"))
sleep_factor = float(
    ssm.mock_ssm_param(
        "/alice/monitoring/gc-loc/sleep-factor",
        "1"))
full_gc_memory_percentage_threshold = int(
    ssm.mock_ssm_param(
        "/alice/monitoring/gc-log/full-gc/memory/percentage-threshold",
        "20"))
full_gc_event_percentage_threshold = int(
    ssm.mock_ssm_param(
        "/alice/monitoring/gc-log/full-gc/event/percentage-threshold",
        "15"))
full_gc_memory_event_count_threshold = int(
    ssm.mock_ssm_param(
        "/alice/monitoring/gc-log/full-gc/event/count-threshold",
        "10"))
newrelic_account_id = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/newrelic/account-id",
    "1234567890")
newrelic_metric_api_url = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/newrelic/metrics/api/url",
    "https://metric-api.newrelic.com/metric/v1")
newrelic_metric_api_key = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/newrelic/metrics/api/key",
    "KEY0123456789",
    True)  # Type: SecureString
newrelic_insights_api_url = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/newrelic/insights/api/url",
    "https://insights-collector.newrelic.com/v1/accounts/%s/events" % (newrelic_account_id))
newrelic_insights_api_key = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/newrelic/insights/api/key",
    "KEY9876543210",
    True)  # Type: SecureString
elb_target_group_arn = ssm.mock_ssm_param(
    "/alice/monitoring/gc-log/elb/target-group/arn",
    "arn:aws:elasticloadbalancing:sa-east-1:012345678912:targetgroup/alice-test-tg/0bf918cd0f0fb52e")
#   Counters.
full_gc_memory_event_count = 0
full_gc_event_percentage_count = 0
number_of_lines_parsed = 0

# Logging object.
log = logger.setup_logger("log_parser")

# Loop to handle log rotation events (when the log file is truncated).
while True:
    # Ending time: X minutes from now.
    end_time = now + datetime.timedelta(minutes=mark_period)
    log.info("Ready to parse log entries; Starting mark", extra={
        "additional_data": {
            "next_mark_in_minutes": mark_period
        }
    })
    try:
        with open(log_file_name) as f:
            # Seek the end of file.
            if seek_end:
                f.seek(0, 2)
            # Loop to read lines from the log file.
            while True:
                line = f.readline().strip("\n")
                if not line:
                    # Log file was rotated (truncated).
                    if f.tell() > os.path.getsize(log_file_name):
                        f.close()
                        seek_end = False
                        log.info("Log file was truncated", extra={
                            "additional_data": {
                                "log_file_name": log_file_name
                            }
                        })
                        break
                    time.sleep(sleep_factor)
                else:
                    if parse_utils.check_valid_log(line):
                        number_of_lines_parsed += 1
                        if "Times: " in line:
                            total_duration = parse_utils.parse_duration(line, True)
                        else:
                            total_duration = parse_utils.parse_duration(line)
                        log_date, log_time = parse_utils.parse_timestamp(line)
                        log_datetime_utc = dateutils.datetime_to_utc("%sT%s" % (log_date, log_time))
                        log_datetime_epoch_utc = dateutils.datetime_to_epoch("%sT%s" % (log_date, log_time))
                        if "[Full GC" in line:
                            full_gc_event_percentage_count += 1
                            full_gc_cause, freed_memory, total_allocated_memory, memory_unit = parse_utils.parse_full_gc(line)
                            freed_memory_percentage = round((100 * (freed_memory / total_allocated_memory)), 2)
                            log.info("Log entry parsed with success", extra={
                                "additional_data": {
                                    "log_details": {
                                        "type": "GC_FULL",
                                        "cause": full_gc_cause,
                                        "memory_freed": freed_memory,
                                        "memory_total": total_allocated_memory,
                                        "memory_unit": memory_unit,
                                        "memory_freed_up_percentage": freed_memory_percentage,
                                        "total_duration_secs": total_duration,
                                        "log_timestamp_utc": log_datetime_epoch_utc,
                                        "log_entry": line
                                    }
                                }
                            })
                            # Push metric to New Relic.
                            newrelic.push_metric(
                                newrelic_metric_api_url,
                                newrelic_metric_api_key,
                                newrelic.format_metric(
                                    "GC_FULL_FREED_MEMORY",
                                    "gauge",
                                    freed_memory,
                                    log_datetime_epoch_utc,
                                    attr={
                                        "attributes": {
                                            "instance_id": instance_id,
                                            "environment": environment,
                                            "application": application_name,
                                            "memory_freed": freed_memory,
                                            "memory_total": total_allocated_memory,
                                            "memory_unit": memory_unit,
                                            "memory_freed_up_percentage": freed_memory_percentage,
                                            "total_duration_secs": total_duration
                                        }
                                    }))
                            # Push custom event to New Relic.
                            newrelic.push_custom_event(
                                newrelic_insights_api_url,
                                newrelic_insights_api_key,
                                payload=[
                                    {
                                        "eventType": "GC_FULL_FREED_MEMORY",
                                        "account": newrelic_account_id,
                                        "memory_freed": freed_memory,
                                        "memory_total": total_allocated_memory,
                                        "value": 1
                                    }
                                ])
                            if (freed_memory_percentage > full_gc_memory_percentage_threshold):
                                full_gc_memory_event_count += 1
                        elif "[GC pause" in line:
                            gc_pause_action, gc_pause_collector, gc_pause_cause = parse_utils.parse_gc_pauses(line)
                            log.info("Log entry parsed with success", extra={
                                "additional_data": {
                                    "log_details": {
                                        "type": "GC_PAUSE",
                                        "action": gc_pause_action,
                                        "collector": gc_pause_collector,
                                        "cause": gc_pause_cause,
                                        "total_duration_secs": total_duration,
                                        "log_timestamp_utc": log_datetime_epoch_utc,
                                        "log_entry": line
                                    }
                                }
                            })
                            # Push metric to New Relic.
                            newrelic.push_metric(
                                newrelic_metric_api_url,
                                newrelic_metric_api_key,
                                newrelic.format_metric(
                                    "GC_PAUSE",
                                    "gauge",
                                    gc_pause_action,
                                    log_datetime_epoch_utc,
                                    attr={
                                        "attributes": {
                                            "instance_id": instance_id,
                                            "environment": environment,
                                            "application": application_name,
                                            "collector": gc_pause_collector,
                                            "cause": gc_pause_cause,
                                            "total_duration_secs": total_duration
                                        }
                                    }))
                            # Push custom event to New Relic.
                            newrelic.push_custom_event(
                                newrelic_insights_api_url,
                                newrelic_insights_api_key,
                                payload=[
                                    {
                                        "eventType": "GC_PAUSE",
                                        "account": newrelic_account_id,
                                        "action": gc_pause_action,
                                        "collector": gc_pause_collector,
                                        "cause": gc_pause_cause,
                                        "value": 1
                                    }
                                ])
                        elif "[Unloading" not in line:
                            gc_other_action = parse_utils.parse_gc_others(line)
                            log.info("Log entry parsed with success", extra={
                                "additional_data": {
                                    "log_details": {
                                        "type": "GC_OTHER",
                                        "action": gc_other_action,
                                        "log_timestamp_utc": log_datetime_epoch_utc,
                                        "total_duration_secs": total_duration,
                                        "log_entry": line
                                    }
                                }
                            })
                            # Push metric to New Relic.
                            newrelic.push_metric(
                                newrelic_metric_api_url,
                                newrelic_metric_api_key,
                                newrelic.format_metric(
                                    "GC_OTHER",
                                    "gauge",
                                    gc_other_action,
                                    log_datetime_epoch_utc,
                                    attr={
                                        "attributes": {
                                            "instance_id": instance_id,
                                            "environment": environment,
                                            "application": application_name,
                                            "total_duration_secs": total_duration
                                        }
                                    }))
                            # Push custom event to New Relic.
                            newrelic.push_custom_event(
                                newrelic_insights_api_url,
                                newrelic_insights_api_key,
                                payload=[
                                    {
                                        "eventType": "GC_OTHER",
                                        "account": newrelic_account_id,
                                        "action": gc_other_action,
                                        "value": 1
                                    }
                                ])
                        else:
                            log.error("Unsupported log entry", extra={
                                "additional_data": {
                                    "log_entry": line
                                }
                            })
                            # Push metric to New Relic.
                            newrelic.push_metric(
                                newrelic_metric_api_url,
                                newrelic_metric_api_key,
                                newrelic.format_metric(
                                    "GC_UNSUPPORTED",
                                    "gauge",
                                    1,
                                    log_datetime_epoch_utc,
                                    attr={
                                        "attributes": {
                                            "instance_id": instance_id,
                                            "environment": environment,
                                            "application": application_name
                                        }
                                    }))
                            # Push custom event to New Relic.
                            newrelic.push_custom_event(
                                newrelic_insights_api_url,
                                newrelic_insights_api_key,
                                payload=[
                                    {
                                        "eventType": "GC_UNSUPPORTED",
                                        "account": newrelic_account_id,
                                        "value": 1
                                    }
                                ])
                        # Time marker has been hit.
                        # Decisions will be make based in the collected metrics.
                        if datetime.datetime.now() >= end_time:
                            log.info("%s minute mark" % (mark_period), extra={
                                "additional_data": {
                                    "number_of_lines_parsed": number_of_lines_parsed
                                }})
                            now = datetime.datetime.now()
                            end_time = now + datetime.timedelta(minutes=mark_period)
                            percentage_of_full_gc_events = (100 * (full_gc_event_percentage_count / number_of_lines_parsed))
                            # Initial conditions for Tomcat restart have been met.
                            if (full_gc_memory_event_count > full_gc_memory_event_count_threshold) and (percentage_of_full_gc_events > full_gc_memory_event_count_threshold):
                                # If all targets in the target group are healthy, Tomcat will be restarted on this node.
                                if elbv2.check_health(elb_target_group_arn, True):
                                    """
                                    Tomcat's restart implementation would be in this part.
                                    Since I don't have additional details on how Tomcat is configured and how its
                                    service is being controlled, I am skipping this implementation.
                                    This part could take leverage of subprocess.run, catching the output from the
                                    command that was executed.
                                    """
                                    # Logs the restart
                                    log.warning("Restarting Tomcat on this node", extra={
                                        "additional_data": {
                                            "count_memory_freed_below_20_percent": full_gc_memory_event_count,
                                            "percentage_full_gc_events": percentage_of_full_gc_events,
                                            "no_unhealthy_hosts": "true"
                                        }
                                    })
                                    # ... and push a metric to New Relic
                                    newrelic.push_metric(
                                        newrelic_metric_api_url,
                                        newrelic_metric_api_key,
                                        newrelic.format_metric(
                                            "TOMCAT_RESTART_OK",
                                            "gauge",
                                            1,
                                            log_datetime_epoch_utc,
                                            attr={
                                                "attributes": {
                                                    "instance_id": instance_id,
                                                    "environment": environment,
                                                    "application": application_name,
                                                    "count_memory_freed_below_20_percent": full_gc_memory_event_count,
                                                    "percentage_full_gc_events": percentage_of_full_gc_events,
                                                    "no_unhealthy_hosts": "true"
                                                }
                                            }))
                                    pass
                                else:
                                    log.error("Insufficient conditions to restart Tomcat on this node", extra={
                                        "additional_data": {
                                            "count_memory_freed_below_20_percent": full_gc_memory_event_count,
                                            "percentage_full_gc_events": percentage_of_full_gc_events,
                                            "no_unhealthy_hosts": "false"
                                        }
                                    })
                                    # We should have metrics to check how many Tomcat restarts
                                    # weren't issued because there were unhealthy hosts.
                                    newrelic.push_metric(
                                        newrelic_metric_api_url,
                                        newrelic_metric_api_key,
                                        newrelic.format_metric(
                                            "TOMCAT_RESTART_FAIL",
                                            "gauge",
                                            1,
                                            log_datetime_epoch_utc,
                                            attr={
                                                "attributes": {
                                                    "instance_id": instance_id,
                                                    "environment": environment,
                                                    "application": application_name,
                                                    "count_memory_freed_below_20_percent": full_gc_memory_event_count,
                                                    "percentage_full_gc_events": percentage_of_full_gc_events,
                                                    "no_unhealthy_hosts": "false"
                                                }
                                            }))
                                    pass
                            else:
                                log.info("Tomcat restart not needed at this time, conditions below thresholds", extra={
                                    "additional_data": {
                                        "count_memory_freed_below_20_percent": full_gc_memory_event_count,
                                        "percentage_full_gc_events": percentage_of_full_gc_events
                                    }
                                })
                            full_gc_memory_event_count = 0
                    else:
                        # No known GC log entry, so just the error message
                        # will be displayed.
                        # No metric will be pushed to New Relic.
                        log.error("Unsupported log entry", extra={
                            "additional_data": {
                                "log_entry": line
                            }
                        })
    except FileNotFoundError:
        # If the log file is not found, register the error and raise the exception.
        log.error("Cannot open log file!", extra={
            "additional_data": {
                "log_file_name": log_file_name
            }
        })
        raise
