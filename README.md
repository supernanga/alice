# ALICE DevOps coding challenge

Hi there! This is my implementation for ALICE's DevOps coding challenge. I hope you guys enjoy it ;).

# Available moving parts

## The log generator ("sampler")
The log generator is a simple implementation that consumes a text file with log entries found on ALICE's DevOps coding challenge PDF file.
It generates entries in `gc.log` file, making it easier to develop and test the main GC log parser script.
By default, the log generator generates appends a new log entry in `gc.log` every `0.5` seconds.

## The garbage collection parsing script ("gc_log_parser")
This is the main script that is responsible to do the magic with a JVM garbage collection log file.

### Why JSON log output?
The garbage collection parsing script outputs everything in JSON. This makes sense since its usage is targeted for the Cloud, preferably AWS.
That being said, any EC2 instance running this script could have its output redirected into a log file that is consumed by CloudWatch agent, which will push every log message into a CloudWatch log group.
This makes easier to ingest into Elasticsearch or even to perform queries using CloudWatch Log Insights.

Enough said, let's check some outputs!

When the script initializes, you will be seeing a lot of SSM messages in the log output. All SSM calls were mocked using `moto`, so that's why you will see `PutParameter` actions in the log output:
```
{
  "level": "INFO",
  "log_facility_name": "ssm_mock",
  "message": "Putting SSM parameter",
  "time": "2020-11-29T23:56:12.469Z",
  "epoch_time": 1606694172.4686913,
  "ssm_parameter_name": "/alice/apps/app1/name",
  "ssm_parameter_type": "String"
}
{
  "level": "INFO",
  "log_facility_name": "ssm_mock",
  "message": "Getting SSM parameter",
  "time": "2020-11-29T23:56:12.471Z",
  "epoch_time": 1606694172.4709332,
  "ssm_parameter_name": "/alice/apps/app1/name",
  "ssm_parameter_type": "String"
}
```

When a valid log entry is parsed, you will see two log outputs (first is from the log parsing and the second is the metric push to New Relic):
```
{
  "level": "INFO",
  "log_facility_name": "log_parser",
  "message": "Log entry parsed with success",
  "time": "2020-11-30T00:05:27.428Z",
  "epoch_time": 1606694727.4278157,
  "log_details": {
    "type": "GC_FULL",
    "cause": "Allocation Failure",
    "memory_freed": 8978,
    "memory_total": 10200,
    "memory_unit": "M",
    "memory_freed_up_percentage": 88.02,
    "total_duration_secs": "5.4572036",
    "log_timestamp_utc": 1488406097,
    "log_entry": "2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure) 10198M->1220M(10200M), 5.4572036 secs]"
  }
}
{
  "level": "INFO",
  "log_facility_name": "new_relic",
  "message": "Pushing metric to New Relic",
  "time": "2020-11-30T00:05:27.428Z",
  "epoch_time": 1606694727.4280164,
  "status": "Not implemented, no New Relic account",
  "new_relic_api_url": "https://metric-api.newrelic.com/metric/v1",
  "new_relic_api_key": "KEY01***",
  "new_relic_api_payload": [
    {
      "metrics": [
        {
          "name": "GC_FULL_FREED_MEMORY",
          "type": "gauge",
          "value": 8978,
          "timestamp": 1488406097,
          "attributes": {
            "instance_id": "i-777e4af9d84b363d6",
            "environment": "sandbox",
            "application": "Astronotus ocellatus",
            "memory_freed": 8978,
            "memory_total": 10200,
            "memory_unit": "M",
            "memory_freed_up_percentage": 88.02,
            "total_duration_secs": "5.4572036"
          }
        }
      ]
    }
  ]
}
```
If an unsupported log entry hits the parser, it will output an error message and also a metric will be pushed to New Relic:
```
{
  "level": "ERROR",
  "log_facility_name": "log_parser",
  "message": "Unsupported log entry",
  "time": "2020-11-30T00:48:53.847Z",
  "epoch_time": 1606697333.846558,
  "log_entry": "2017-03-01T21:42:43.865-0500: 23156.537: [Unloading, 0.0527561 secs], 0.0710453 secs]"
}
{
  "level": "INFO",
  "log_facility_name": "new_relic",
  "message": "Pushing metric to New Relic",
  "time": "2020-11-30T00:48:53.847Z",
  "epoch_time": 1606697333.8467336,
  "status": "Not implemented, no New Relic account",
  "new_relic_api_url": "https://metric-api.newrelic.com/metric/v1",
  "new_relic_api_key": "KEY01***",
  "new_relic_api_payload": [
    {
      "metrics": [
        {
          "name": "GC_UNSUPPORTED",
          "type": "gauge",
          "value": 1,
          "timestamp": 1488422563,
          "attributes": {
            "instance_id": "i-2275d6ac23f3f8baa",
            "environment": "sandbox",
            "application": "Astronotus ocellatus"
          }
        }
      ]
    }
  ]
}
```
After a metric is pushed to New Relic, a custom event is also pushed. Example:
```
{
  "level": "INFO",
  "log_facility_name": "new_relic",
  "message": "Pushing custom event to New Relic",
  "time": "2020-11-30T01:25:09.171Z",
  "epoch_time": 1606699509.1706197,
  "status": "Not implemented, no New Relic account",
  "new_relic_api_url": "https://insights-collector.newrelic.com/v1/accounts/1234567890/events",
  "new_relic_api_key": "KEY98***",
  "new_relic_api_payload": [
    {
      "eventType": "GC_PAUSE",
      "account": "1234567890",
      "action": "G1 Evacuation Pause",
      "collector": "mixed",
      "cause": "null",
      "value": 1
    }
  ]
}
```

When a given minute mark is hit (in this example, it was set to `1` minute), Tomcat could be restarted if all the conditions and thresholds are met:
```
{
  "level": "INFO",
  "log_facility_name": "log_parser",
  "message": "1 minute mark",
  "time": "2020-11-30T00:05:29.841Z",
  "epoch_time": 1606694729.841365,
  "number_of_lines_parsed": 241
}
{
  "level": "INFO",
  "log_facility_name": "elbv2",
  "message": "ELBv2 target group health status check",
  "time": "2020-11-30T00:05:29.842Z",
  "epoch_time": 1606694729.8415132,
  "target_group_arn": "arn:aws:elasticloadbalancing:sa-east-1:012345678912:targetgroup/alice-test-tg/0bf918cd0f0fb52e",
  "number_of_targets": 1
}
{
  "level": "INFO",
  "log_facility_name": "elbv2",
  "message": "ELBv2 target group health status check completed with success",
  "time": "2020-11-30T00:05:29.842Z",
  "epoch_time": 1606694729.841599,
  "target_group_arn": "arn:aws:elasticloadbalancing:sa-east-1:012345678912:targetgroup/alice-test-tg/0bf918cd0f0fb52e",
  "number_of_targets": 1,
  "unhealthy_count": 0
}
{
  "level": "WARNING",
  "log_facility_name": "log_parser",
  "message": "Restarting Tomcat on this node",
  "time": "2020-11-30T00:05:29.842Z",
  "epoch_time": 1606694729.8419712,
  "count_memory_freed_below_20_percent": 84,
  "percentage_full_gc_events": 100,
  "no_unhealthy_hosts": "true"
}
{
  "level": "INFO",
  "log_facility_name": "new_relic",
  "message": "Pushing metric to New Relic",
  "time": "2020-11-30T00:05:29.842Z",
  "epoch_time": 1606694729.8420687,
  "status": "Not implemented, no New Relic account",
  "new_relic_api_url": "https://metric-api.newrelic.com/metric/v1",
  "new_relic_api_key": "KEY01***",
  "new_relic_api_payload": [
    {
      "metrics": [
        {
          "name": "TOMCAT_RESTART_OK",
          "type": "gauge",
          "value": 1,
          "timestamp": 1488406157,
          "attributes": {
            "instance_id": "i-777e4af9d84b363d6",
            "environment": "sandbox",
            "application": "Astronotus ocellatus",
            "count_memory_freed_below_20_percent": 84,
            "percentage_full_gc_events": 100,
            "no_unhealthy_hosts": "true"
          }
        }
      ]
    }
  ]
}
```


# How to run
First, install required dependencies:
```
pip install -r requirements.txt
```
Then, start the sampler:
```
python ./sampler.py
```
Sample output:
```
Generating random GC logs based on sample file...
  [o] 109 bytes written to gc.log, 918385 bytes total, 7458 lines written
```

And finally, start the garbage collection parsing script:
```
python ./gc_log_parser.py
```

# Lessons learned
I learned some lessons while doing ALICE's coding challenge. Below is a quick list from the lessons that I learned during its implementation:

1. Last time that I used regular expressions, Perl and "cgi-bin" stuff were pretty much like AWS is today ("the" thing). I need to recap all my studies for regular expressions.

2. I need to test and study `moto` a little bit more. It is an interesting piece of software.

3. Python studies should be resumed: I need to improve my Python skills.

4. If you don't need the Cloud during an implementation like this, take the risk and mock the stuff! This is a cost effective approach since I am trying to reduce the bill from my personal AWS account.

# What isn't implemented and why?
1. Notifications via Slack: This could be done using https://pypi.org/project/slackclient/, as I already did in the past using SNS and Lambda. From the point of view for this implementation, when a metric is pushed to New Relic a message would be pushed into a SNS topic (coming from a SSM parameter, for example), which will trigger a Lambda function that pushes notifications over a given Slack channel. SSM parameter store would be used to store configurations for this Lambda (Slack token [`SecureString`], Channel to publish [`String`]), and so on).

2. Better regular expressions: As I stated above, I need to recap and study regular expressions again. But what I did for this implementation did the job, but I think that there is room for improvement.

# References
* https://github.com/spulec/moto
* https://blog.gceasy.io/2016/02/22/understand-garbage-collection-log/
* https://dev.to/sematext/java-garbage-collection-logs-how-to-analyze-them-4hgb
* https://docs.newrelic.com/docs/telemetry-data-platform/ingest-manage-data/ingest-apis/report-metrics-metric-api
* https://docs.newrelic.com/docs/telemetry-data-platform/ingest-manage-data/understand-data/metric-data-type
* https://docs.newrelic.com/docs/telemetry-data-platform/ingest-manage-data/ingest-apis/use-event-api-report-custom-events

# Author
* Thiago Basilio (nanga@nanga.com.br)