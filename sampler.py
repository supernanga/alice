#!/usr/bin/env python
"""
Simple sampler to generate a test-purpose GC log file.
"""
import random
import time
import itertools


# Populate the variables.
sample_file = "samples.txt"
log_file = "gc.log"
sleep_amount = 0.5
total_bytes = 0
number_of_lines_written = 0
spinner = itertools.cycle(['.', 'o', 'Ö', 'O'])

print("Generating random GC logs based on sample file...")
while True:
    lines = open(sample_file).read().splitlines()
    random_line = random.choice(lines)
    # Generate random GC logs from the sample file, appending then
    # into GC's log file.
    with open(log_file, 'a') as log_file_object:
        # Ignore commented out lines or real comments.
        if not random_line.lstrip().startswith("#"):
            log_file_object.write("%s\n" % (random_line))
            amount_bytes = len(random_line)
            total_bytes = total_bytes + amount_bytes
            number_of_lines_written += 1
            # Prints a nice status for the sampler, with a moving
            # progress indicator.
            print("\r  [%s] %s bytes written to %s, %s bytes total, %s lines written" % (
                next(spinner),
                amount_bytes,
                log_file,
                total_bytes,
                number_of_lines_written), end="", flush=True)
    time.sleep(sleep_amount)
